package com.project.spring;

import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import org.springframework.beans.factory.annotation.Autowired;

@Route("")
@Theme(Lumo.class)
@HtmlImport("styles/shared-styles.html")
public class MainView extends VerticalLayout {

    public MainView(@Autowired TextAnalyzerBean bean) {

        TextArea offsetHeading = new TextArea("Offset");
        offsetHeading.setReadOnly(true);
        offsetHeading.setWidthFull();
        AnalyzerFormTemplate sanitizerForm = new AnalyzerFormTemplate(offsetHeading, bean);

        add(offsetHeading);
        add(sanitizerForm);

    }

}
