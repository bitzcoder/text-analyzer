package com.project.spring;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.textfield.TextArea;

public class AnalyzerFormTemplate extends FormLayout {

    private TextArea originalText = new TextArea("Original Text");
    private TextArea  cleanedText = new TextArea("Cleaned Text");
    private Button processTextButton = new Button("Get Offset");
    private String offset;
    
    public AnalyzerFormTemplate(TextArea offsetHeading, TextAnalyzerBean bean){
        add(originalText,cleanedText);
        add(processTextButton);
        this.offset = "";

        processTextButton.addClickListener(buttonClickEvent -> {
            this.offset = bean.setOffset(originalText.getValue(), cleanedText.getValue());
            offsetHeading.setValue(this.offset);
            Notification.show(offset);
        });
    }
}