package com.project.spring;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TextAnalyzerBean {

    /**
     * method determines the corresponding offset in the cleaned text.
     * @param originalText
     * @param cleanedText
     * @return
     */
    public String setOffset(String originalText, String cleanedText){

        String missingString = "";

        List<Character> alreadyFound = new ArrayList<>();
        int characterCount[] = new int[255];

        for(int i=0; i < originalText.length(); i++ ){
            char currentChar = originalText.charAt(i);
            characterCount[(int)currentChar] += 1;
            if(cleanedText.indexOf(currentChar) == -1){
                missingString += currentChar;
            }else if((alreadyFound.contains(originalText.charAt(i)) && (characterCount[(int)currentChar] > countCharacterFreq(cleanedText, currentChar)))) {
                missingString += currentChar;
            }else{
                alreadyFound.add(currentChar);
            }
        }

        return missingString;
    }

    private int countCharacterFreq(String cleanedText, Character selectedChar){
        int counter =0;
        for(int i=0; i < cleanedText.length(); i++ ){
            if(cleanedText.charAt(i) == selectedChar){
                counter++;
            }
        }

        return counter;
    }
}
